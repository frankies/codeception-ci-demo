// assets/js/app.js
import Vue from 'vue';
import Hello from './components/Hello'

/**
 * Create a fresh Vue Application instance
 */
new Vue({
    el: '#app',
    // render: h => h(Hello)
    components: {Hello},
});
