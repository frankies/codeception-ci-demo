# Running codeception web tests with gitlab-ci

I created this demo project because I have struggled a lot to get this working on another project I work on.
Initially I just wanted to run web tests, I added running yarn during CI later on.

Disclaimer: I am just cluelessly trying things out. If you see mistakes, please let me know.

## The demo application

The application I will be testing, is a very basic vue.js application. 

If you want to try it out, you first need to run yarn:
```
yarn install
yarn encore dev
```
If you don't have yarn, see the [installation instructions](https://yarnpkg.com/lang/en/docs/install/).

Then you can just open the the `public/index.html` page in your browser.

## Testing locally

To run the tests on your local computer, make sure you have a working [composer](https://getcomposer.org/).

### Install the dependencies for codeception:

```
   # run this command from the project root:
   composer install
```

### Generate codeception actor classes:

```
   ./vendor/bin/codecept build
```

### Start a local webserver

E.g. the built in server of php:

```
   php -S 0.0.0.0:8080 -t public/
```

### Run a selenium server

This is easy with docker:

```
   docker run --net=host selenium/standalone-chrome:3.141.59-oxygen
```

(I pinned on this version, because it doesn't seem to work on newer versions. I have no clue why.)

### Run the tests

```
   ./vendor/bin/codecept run acceptance
```

## Testing with gitlab-ci

I created a [`.gitlab-ci`-file](https://gitlab.com/johanv/codeception-ci-demo/blob/master/.gitlab-ci.yml), and this way every time I push to my
repository, a gitlab runner [runs the web tests](https://gitlab.com/johanv/codeception-ci-demo/pipelines). Which is very cool!

There are still some issues with this .gitlab-ci, if you know how I can improve it: let me know.

The main problem is that I had to replace the url for the appliction for the selenium server to find it. This happens in this line:
```
    - sed -i "s/localhost:8080/$LOCAL_IP:8080/" tests/acceptance.suite.yml
```
The tricky part is finding out the local ip address of the container running the test script. I did it like this:
```
    - LOCAL_IP=$(ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}')
```
But for this to work, I need iproute2 on my php container, and guess what: it wasn't there.

So I created a custom php-container which I uploaded to the [registry of the project](https://gitlab.com/johanv/codeception-ci-demo/container_registry).
It is based on the [Dockerfile](https://gitlab.com/johanv/codeception-ci-demo/blob/master/docker/php/Dockerfile) included in the project source.

## A thing you should not try

I tried to use nginx and php-fpm containers for running the webserver for the tests, but this caused all kinds of (docker) networking issues.
So you shouldn't do that. As it turns out, PHP's built-in webserver will do the job just fine.
